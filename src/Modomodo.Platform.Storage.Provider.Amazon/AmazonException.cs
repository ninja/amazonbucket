﻿using System;

namespace Modomodo.Platform.Storage.Provider.Amazon
{
    public class AmazonException : Exception
    {
        public AmazonException(string message)
			: base(message)
		{
		}
        public AmazonException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}