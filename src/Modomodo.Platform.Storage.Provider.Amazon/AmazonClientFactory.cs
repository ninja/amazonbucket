﻿using System;
using Modomodo.Platform.Stack.Interfaces;
using Modomodo.Platform.Storage.Provider.Amazon.Configuration;

namespace Modomodo.Platform.Storage.Provider.Amazon
{
    public class AmazonClientFactory 
    {
        protected AmazonClientFactory()
        {   
        }

        public static AmazonClient CreateAmazonClient(IConfiguration<AmazonSettings> amazonSettings)
        {
            if (amazonSettings == null)
                throw new ArgumentException("amazonSettings");

            var section = amazonSettings.GetSection();
            var accessKey = section.AccessKey;
            var secretAccessKey = section.SecretAccessKey;
            var bucketName = section.BucketName;

            return CreateAmazonClient(accessKey, secretAccessKey, bucketName);
        }

        public static AmazonClient CreateAmazonClient(string accessKey, string secretAccessKey, string bucketName)
        {
            return new AmazonClient( accessKey, secretAccessKey, bucketName);
        }
    }
}