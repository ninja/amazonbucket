﻿using System.Collections.Generic;
using Amazon;
using Modomodo.Platform.Stack.Interfaces.Storage;
using Modomodo.Platform.Storage.Provider.Amazon.Models;

namespace Modomodo.Platform.Storage.Provider.Amazon
{
    public class AmazonClient : AmazonClientNative, ISpecificOperationsAmazonS3
    {
        public AmazonClient(string accessKey, string secretAccessKey, string bucketName)
        {
            SetAccessKey(accessKey);
            SetSecretAccessKey(secretAccessKey);
            SetBucketName(bucketName);
            CreateClient( accessKey, secretAccessKey);
        }

        private void CreateClient(string accessKey, string secretAccessKey)
        {
            AmazonS3 = AWSClientFactory.CreateAmazonS3Client(accessKey, secretAccessKey, RegionEndpoint.EUWest1);
        }

        public void Put(BasicWriteObjectRequest basicWriteObjectRequest)
        {
            if (basicWriteObjectRequest == null)
                throw new AmazonException("BasicWriteObjectRequest can not be null");
            base.PutObject((WriteObjectRequest)basicWriteObjectRequest);
        }

        public byte[] Get(BasicObjectRequest basicObjectRequest)
        {
            if (basicObjectRequest == null)
                throw new AmazonException("BasicObjectRequest can not be null");
            return base.GetObject((ObjectRequest)basicObjectRequest);
        }

        public void Delete(BasicObjectRequest basicObjectRequest)
        {
            if (basicObjectRequest == null)
                throw new AmazonException("BasicObjectRequest can not be null");
            base.DeleteObject((ObjectRequest)basicObjectRequest);
        }

        public IList<string> GetContentsByBucket(string prefix)
        {
            return base.ListObjects(prefix);
        }

        public IList<string> GetContentsByBucket()
        {                                                                               
            return base.ListObjects(string.Empty);
        }

        public void DeleteObjects(string prefix)
        {
            base.DeleteObjects(prefix);
        }

        public void Disposable()
        {
            base.Dispose(true);
        }
    }
}