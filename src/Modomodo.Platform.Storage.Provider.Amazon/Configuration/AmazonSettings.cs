﻿using System.Configuration;
using Modomodo.Platform.Stack.Interfaces;

namespace Modomodo.Platform.Storage.Provider.Amazon.Configuration
{
    public class AmazonSettings : ConfigurationSection, IConfiguration<AmazonSettings>
    {
        public AmazonSettings GetSection()
        {
            return ConfigurationManager.GetSection("AmazonSettings") as AmazonSettings;
        }

        [ConfigurationProperty("UserName", IsRequired = true)]
        public string UserName
        {
            get
            {
                return this["UserName"].ToString();
            }
            set
            {
                this["UserName"] = value;
            }
        }

        [ConfigurationProperty("AccessKey", IsRequired = true)]
        public string AccessKey
        {
            get
            {
                return this["AccessKey"].ToString();
            }
            set
            {
                this["AccessKey"] = value;
            }
        }

        [ConfigurationProperty("SecretAccessKey", IsRequired = true)]
        public string SecretAccessKey
        {
            get
            {
                return this["SecretAccessKey"].ToString();
            }
            set
            {
                this["SecretAccessKey"] = value;
            }
        }

        [ConfigurationProperty("BucketName", IsRequired = true)]
        public string BucketName
        {
            get
            {
                return this["BucketName"].ToString();
            }
            set
            {
                this["BucketName"] = value;
            }
        }
    }
}