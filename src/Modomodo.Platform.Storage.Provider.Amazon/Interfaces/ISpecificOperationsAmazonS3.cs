﻿using System.Collections.Generic;
using Modomodo.Platform.Stack.Interfaces.Storage;

namespace Modomodo.Platform.Storage.Provider.Amazon.Interfaces
{
    public interface ISpecificOperationsAmazonS3 : IBasicOperations
    {
        IList<string> GetContentsByBucket();
        IList<string> GetContentsByBucket(string prefix);
        void DeleteObjects(string prefix);
    }
}
