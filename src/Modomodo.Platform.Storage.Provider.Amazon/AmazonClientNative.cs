﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Amazon.S3;
using Amazon.S3.Model;
using Modomodo.Platform.Storage.Provider.Amazon.Models;

namespace Modomodo.Platform.Storage.Provider.Amazon
{
    public abstract class AmazonClientNative : IDisposable
    {
        public string AccessKey { get; private set; }
        public string SecretAccessKey { get; private set; }
        public string BucketName { get; private set; }
        protected IAmazonS3 AmazonS3 { get; set; }

        internal void PutObject(WriteObjectRequest writeObjectRequest)
        {
            try
            {
                using (var memoryStream = new MemoryStream(writeObjectRequest.Source))
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = BucketName,
                        ContentType = writeObjectRequest.MediaType,
                        Key = writeObjectRequest.GetKey(),
                        CannedACL = MapperAcl(writeObjectRequest.Acl)
                    };
                    if (writeObjectRequest.Encryption)
                        request.ServerSideEncryptionMethod = ServerSideEncryptionMethod.AES256;
                    request.InputStream = memoryStream;
                    AmazonS3.PutObject(request);
                }
            }
            catch (Exception exception)
            {
                throw new AmazonS3Exception("Error put", exception);
            }
        }

        internal Byte[] GetObject(ObjectRequest objectRequest)
        {
            try
            {
                var request = new GetObjectRequest {BucketName = BucketName, Key = objectRequest.GetKey()};
                var response = AmazonS3.GetObject(request);
                using (var memoryStream = new MemoryStream())
                {
                    response.ResponseStream.CopyTo(memoryStream);
                    return memoryStream.ToArray();
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode.Equals("NoSuchKey"))
                    return null;
            }
            return null;
        }

        internal void DeleteObject(ObjectRequest objectRequest)
        {
            try
            {
                var request = new DeleteObjectRequest {BucketName = BucketName, Key = objectRequest.GetKey()};
                AmazonS3.DeleteObject(request);
            }
            catch (Exception exception)
            {
                throw new AmazonS3Exception("Delete object", exception);
            }
        }

        internal IList<string> ListObjects( string prefix)
        {
            var listObjects = new List<string>();
            try
            {
                var request = new ListObjectsRequest {BucketName = BucketName};
                if (!string.IsNullOrWhiteSpace(prefix))
                    request.Prefix = prefix;
                do
                {
                    var response = AmazonS3.ListObjects(request);
                    listObjects.AddRange(response.S3Objects.Select(obj => obj.Key));
                    if (response.IsTruncated)
                        request.Marker = response.NextMarker;
                    else
                        request = null;
                } while (request != null);
            }
            catch (Exception exception)
            {
                throw new AmazonS3Exception("Get list object", exception);
            }
            return listObjects;
        }

        internal void DeleteObjects(string prefix)
        {
            var listObject = ListObjects(prefix);
            if (listObject.Count <= 0) return;
            var deleteObjects = listObject.Select(key => new KeyVersion { Key = key }).ToList();
            try
            {
                var splitDeleteObject =
                           deleteObjects.Select((x, i) => new { Index = i, Value = x })
                           .GroupBy(x => x.Index / 1000)
                           .Select(x => x.Select(v => v.Value).ToList())
                           .ToList();

                foreach (var deleteObject in splitDeleteObject)
                {
                    var multiObjectDeleteRequest = new DeleteObjectsRequest
                    {
                        BucketName = BucketName,
                        Objects = deleteObject
                    };
                    AmazonS3.DeleteObjects(multiObjectDeleteRequest);
                }
            }
            catch( Exception exception)
            {
                throw new AmazonS3Exception("Delete list object", exception);
            }            
        }

        public void SetAccessKey(string accessKey)
        {
            if (string.IsNullOrWhiteSpace(accessKey))
                throw new AmazonException("AccessKey can not be null");

            AccessKey = accessKey;
        }

        public void SetSecretAccessKey(string secretAccessKey)
        {
            if (string.IsNullOrWhiteSpace(secretAccessKey))
                throw new AmazonException("SecretAccessKey can not be null");

            SecretAccessKey = secretAccessKey;
        }

        public void SetBucketName(string bucketName)
        {
            if (string.IsNullOrWhiteSpace(bucketName))
                throw new AmazonException("BucketName can not be null");

            BucketName = bucketName;
        }

        private S3CannedACL MapperAcl(Acl acl)
        {
            switch (acl)
            {
                case Acl.AuthenticatedRead:
                    return S3CannedACL.AuthenticatedRead;
                case Acl.NoACL:
                    return S3CannedACL.NoACL;
                case Acl.Private:
                    return S3CannedACL.Private;
                case Acl.PublicRead:
                    return S3CannedACL.PublicRead;
                case Acl.PublicReadWrite:
                    return S3CannedACL.PublicReadWrite;
                default:
                    return S3CannedACL.PublicRead;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;
            if (AmazonS3 != null)
                AmazonS3.Dispose();
        }
    }
}