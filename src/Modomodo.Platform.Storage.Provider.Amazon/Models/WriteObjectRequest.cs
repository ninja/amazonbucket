﻿using System;
using Modomodo.Platform.Stack.Interfaces.Storage;

namespace Modomodo.Platform.Storage.Provider.Amazon.Models
{
    public sealed class WriteObjectRequest : BasicWriteObjectRequest
    {
        public Acl Acl { get; private set; }
        public bool Encryption { get; set; }
        public string MediaType { get; private set; }

        public WriteObjectRequest(string name, string extension, string base64Source, string mediaType = null)
        {
            SetBase64Source(base64Source);
            Init(name, extension, Source);
            SetMediaTypeProperty(mediaType);
        }

        public WriteObjectRequest(string name, string extension, byte[] source, string mediaType = null)
        {
            Init(name, extension, source);
            SetMediaTypeProperty(mediaType);
        }

        private void SetMediaTypeProperty(string mediaType)
        {
            if (!string.IsNullOrWhiteSpace(mediaType))
                this.MediaType = mediaType;
            else
            {
                string tryMediaType;
                if(!HelperMediaType.ExtensionFileToMedia.TryGetValue(Extension.ToLower(), out tryMediaType))
                    throw new ArgumentException("Media-Type not recognized. Pass mediaType parameter via constructor");

                this.MediaType = tryMediaType;
            }
        }

        public void SetBase64Source(string base64Source)
        {
            if (string.IsNullOrWhiteSpace(base64Source))
                throw new ArgumentNullException("base64Source");
            Source = Convert.FromBase64String(base64Source);
        }

        public void SetAcl(Acl acl)
        {
            Acl = acl;
        }

        public void SetEncyption()
        {
            Encryption = true;
        }
    }
}