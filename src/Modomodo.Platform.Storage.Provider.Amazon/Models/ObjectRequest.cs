﻿using Modomodo.Platform.Stack.Interfaces.Storage;

namespace Modomodo.Platform.Storage.Provider.Amazon.Models
{
    public sealed class ObjectRequest : BasicObjectRequest
    {
        public ObjectRequest(string name, string extension)
        {
            Init(name, extension);
        }
    }
}