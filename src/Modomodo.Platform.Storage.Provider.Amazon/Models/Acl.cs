﻿namespace Modomodo.Platform.Storage.Provider.Amazon.Models
{
    public enum Acl
    {
        NoACL = 0,
        Private = 1,
        PublicRead = 2,
        PublicReadWrite = 3,
        AuthenticatedRead = 4,
    }
}