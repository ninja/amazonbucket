﻿using System.Collections.Generic;
using System.Net.Mime;

namespace Modomodo.Platform.Storage.Provider.Amazon
{
    //TODO: da eliminare
    public class HelperMediaType
    {
        internal static Dictionary<string, string> ExtensionFileToMedia;

        static HelperMediaType()
        {
            ExtensionFileToMedia = new Dictionary<string, string>();
            ExtensionFileToMedia[".xls"] = Application.MicrosoftExcel;
            ExtensionFileToMedia[".ppt"] = Application.MicrosoftPowerpoint;
            ExtensionFileToMedia[".doc"] = Application.MicrosoftWord;
            ExtensionFileToMedia[".pdf"] = Application.Pdf;
            ExtensionFileToMedia[".ps"] = Application.Postscript;
            ExtensionFileToMedia[".rtf"] = Application.Rtf;
            ExtensionFileToMedia[".xml"] = Application.Soap;
            ExtensionFileToMedia[".zip"] = Application.Zip;
            ExtensionFileToMedia[string.Empty] = Application.Octet;

            ExtensionFileToMedia[".bmp"] = Image.Bmp;
            ExtensionFileToMedia[".dwg"] = Image.Dwg;
            ExtensionFileToMedia[".gif"] = Image.Gif;
            ExtensionFileToMedia[".jpeg"] = Image.Jpeg;
            ExtensionFileToMedia[".jpg"] = Image.Jpeg;
            ExtensionFileToMedia[".png"] = Image.Png;
            ExtensionFileToMedia[".tiff"] = Image.Tiff;

            ExtensionFileToMedia[".eml"] = Message.Rfc822;

            ExtensionFileToMedia[".csv"] = Text.Csv;
            ExtensionFileToMedia[".html"] = Text.Html;
            ExtensionFileToMedia[".txt"] = Text.Plain;
            ExtensionFileToMedia[".rtf"] = Text.RichText;
            ExtensionFileToMedia[".xml"] = Text.Xml;
            ExtensionFileToMedia[".json"] = Text.Json;
        }

        /// <summary>
        /// Application-oriented media types
        /// </summary>
        public static class Application
        {
            /// <summary>
            /// PDF format
            /// </summary>
            public const string Pdf = MediaTypeNames.Application.Pdf;

            /// <summary>
            /// RTF format
            /// </summary>
            public const string Rtf = MediaTypeNames.Application.Rtf;

            /// <summary>
            /// SOAP media-type
            /// </summary>
            public const string Soap = MediaTypeNames.Application.Soap;

            /// <summary>
            /// Zip format
            /// </summary>
            public const string Zip = MediaTypeNames.Application.Zip;

            /// <summary>
            /// Microsoft Word format
            /// </summary>
            public const string MicrosoftWord = "application/msword";

            /// <summary>
            /// Microsoft Excel format
            /// </summary>
            public const string MicrosoftExcel = "application/vnd.ms-excel";

            /// <summary>
            /// Microsoft Powerpoint format
            /// </summary>
            public const string MicrosoftPowerpoint = "application/vnd.ms-powerpoint";

            /// <summary>
            /// Postscript format
            /// </summary>
            public const string Postscript = "application/postscript";

            /// <summary>
            /// Generic binary media-type
            /// </summary>
            public const string Octet = MediaTypeNames.Application.Octet;
        }

        /// <summary>
        /// Image-oriented media types
        /// </summary>
        public static class Image
        {
            /// <summary>
            /// PNG format
            /// </summary>
            public const string Png = "image/png";

            /// <summary>
            /// BMP format
            /// </summary>
            public const string Bmp = "image/bmp";

            /// <summary>
            /// DWG format
            /// </summary>
            public const string Dwg = "image/dwg";

            /// <summary>
            /// JPEG format
            /// </summary>
            public const string Jpeg = MediaTypeNames.Image.Jpeg;

            /// <summary>
            /// Tiff format
            /// </summary>
            public const string Tiff = MediaTypeNames.Image.Tiff;

            /// <summary>
            /// GIF format
            /// </summary>
            public const string Gif = MediaTypeNames.Image.Gif;
        }

        /// <summary>
        /// Text-oriented media types
        /// </summary>
        public static class Text
        {
            /// <summary>
            /// Plain text
            /// </summary>
            public const string Plain = MediaTypeNames.Text.Plain;

            /// <summary>
            /// HTML format
            /// </summary>
            public const string Html = MediaTypeNames.Text.Html;

            /// <summary>
            /// Rich Text format
            /// </summary>
            public const string RichText = MediaTypeNames.Text.RichText;

            /// <summary>
            /// XML format
            /// </summary>
            public const string Xml = MediaTypeNames.Text.Xml;

            /// <summary>
            /// CSV format
            /// </summary>
            public const string Csv = "text/csv";

            /// <summary>
            /// JSON format
            /// </summary>
            public const string Json = "application/json";
        }

        /// <summary>
        /// Message-oriented media-types
        /// </summary>
        public static class Message
        {
            /// <summary>
            /// RFC822 format
            /// </summary>
            public const string Rfc822 = "message/rfc822";
        }
    }
}
