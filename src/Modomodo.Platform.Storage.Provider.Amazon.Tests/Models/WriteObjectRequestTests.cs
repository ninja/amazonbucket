﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Platform.Stack.Interfaces.Storage;
using Modomodo.Platform.Storage.Provider.Amazon.Models;

namespace Modomodo.Platform.Storage.Provider.Amazon.Tests.Models
{
    [TestClass]
    public class WriteObjectRequestTests
    {
        [TestMethod]
        public void WriteObjectRequest_Should_Be_Of_Type_BasicObjectRequest()
        {
            var writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, new byte[10]);

            Assert.IsInstanceOfType(writeObjectRequest, typeof(BasicObjectRequest));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void WriteObjectRequest_With_Wrong_Extension_Return_ArgumentException()
        {
            var writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, ".fake", new byte[10]);
        }

        [TestMethod]
        public void WriteObjectRequest_Byte_WithCustomMediaType_VerifySettedRightProperty()
        {
            var writeobject = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, new byte[10],
                Helpers.MediaType);

            Assert.AreEqual(writeobject.MediaType, Helpers.MediaType);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void WriteObjectRequest_WithMediaType_WithoutExtension_ReturnArgumentNullException()
        {
            new WriteObjectRequest(Helpers.NameFile, string.Empty, new byte[10],Helpers.MediaType);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void WriteObjectRequest_WithoutExtension_ReturnArgumentNullException()
        {
            new WriteObjectRequest(Helpers.NameFile, string.Empty, new byte[10]);
        }

        [TestMethod]
        public void WriteObjectRequest_Base64_WithCustomMediaType_VerifySettedRightProperty()
        {
            var writeobject = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, Helpers.SampleBase64,Helpers.MediaType);
            Assert.AreEqual(writeobject.MediaType, Helpers.MediaType);
        }


        [TestMethod]
        public void WriteObjectRequest_Base64_WithoutCustomMediaType_VerifyMediaTypePropertyIsRightSetted()
        {
            var writeobject = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, Helpers.SampleBase64);
            Assert.AreEqual(writeobject.MediaType, HelperMediaType.Image.Jpeg);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void WriteObjectRequest_With_Base64Source_Empty_Return_ArgumentNullException()
        {
            var writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, string.Empty);
        }

        [TestMethod]
        public void GetKey_Without_Path_Return_Name_Extension()
        {
            var writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, new byte[10]);
            var key = writeObjectRequest.GetKey();
            const string result = Helpers.NameFile + Helpers.Extension;

            Assert.IsTrue( key.Equals( result));
        }

        [TestMethod]
        public void GetKey_With_Path_Return_Path_Name_Extension()
        {
            var pathFolder = "folder";
            var writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, new byte[10])
                {
                    PathFolder = pathFolder
                };
            var key = writeObjectRequest.GetKey();
            var result = string.Format("{0}/{1}{2}", pathFolder, Helpers.NameFile, Helpers.Extension);

            Assert.IsTrue( key.Equals( result));

            pathFolder = "folder/subfolder/";
            writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, new byte[10])
                {
                    PathFolder = pathFolder
                };
            key = writeObjectRequest.GetKey();
            result = pathFolder + Helpers.NameFile + Helpers.Extension;

            Assert.IsTrue(key.Equals(result));
        }

        [TestMethod]
        public void Check_ToString()
        {
            var writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, new byte[10]);

            var result = string.Format("Path: {0} - Name: {1} - Extension: {2}", writeObjectRequest.PathFolder,
                                       Helpers.NameFile, Helpers.Extension);

            Assert.IsTrue(writeObjectRequest.ToString().Equals(result));
        }

        [TestMethod]
        public void SetAcl_Should_Be_Default_NoACL()
        {
            var writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, new byte[10]);
            Assert.IsTrue( writeObjectRequest.Acl == Acl.NoACL);
        }

        [TestMethod]
        public void Encryption_Should_Be_Default_False()
        {
            var writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, new byte[10]);
            Assert.IsFalse( writeObjectRequest.Encryption);
        }
    }
}