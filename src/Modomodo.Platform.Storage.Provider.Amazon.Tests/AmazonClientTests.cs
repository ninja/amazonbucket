﻿using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Platform.Stack.Interfaces;
using Modomodo.Platform.Storage.Provider.Amazon.Configuration;
using Modomodo.Platform.Storage.Provider.Amazon.Models;
using NSubstitute;

namespace Modomodo.Platform.Storage.Provider.Amazon.Tests
{
    [TestClass]
    public class AmazonClientTests
    {
        private readonly string _pathResourceJpg;
        private readonly string _baseSource64;
        private readonly IConfiguration<AmazonSettings> _amazonSettings;
        private readonly ObjectRequest _objectRequest;

        private const string AccessKey = "accessKey";
        private const string SecretAccessKey = "secretaccesskey";
        private const string BucketName = "bucketname";

        public AmazonClientTests()
        {
            _pathResourceJpg = Helpers.PathResource( Helpers.GetPartialPath);

            _amazonSettings = Substitute.For<IConfiguration<AmazonSettings>>();
            _amazonSettings.GetSection().Returns(ConfigurationManager.GetSection(Helpers.SectionEon));

            _objectRequest = new ObjectRequest(Helpers.NameFile, Helpers.Extension);
            _baseSource64 = Helpers.Base64(Helpers.GetPartialPath);
        }

        [TestCleanup]
        public void CleanUp()
        {
            DeleteResource();
        }

        [TestMethod]
        public void Check_AmazonClientNative()
        {
            var amazonClientNative = new AmazonClient(AccessKey, SecretAccessKey, BucketName);

            Assert.IsTrue(amazonClientNative.AccessKey.Equals(AccessKey));
            Assert.IsTrue(amazonClientNative.SecretAccessKey.Equals(SecretAccessKey));
            Assert.IsTrue(amazonClientNative.BucketName.Equals(BucketName));
        }

        [ExpectedException(typeof(AmazonException))]
        [TestMethod]
        public void SetAccessKey_With_AccessKey_Empty_Return_AmazonException()
        {
            var amazonClientNative = new AmazonClient(string.Empty,
                SecretAccessKey, BucketName);
        }

        [ExpectedException(typeof(AmazonException))]
        [TestMethod]
        public void SetAccessKey_With_AccessKey_Null_Return_AmazonException()
        {
            var amazonClientNative = new AmazonClient(null,
                SecretAccessKey, BucketName);
        }

        [ExpectedException(typeof(AmazonException))]
        [TestMethod]
        public void SetSecretAccessKey_With_SecretAccessKey_Empty_Return_AmazonException()
        {
            var amazonClientNative = new AmazonClient(AccessKey,
                string.Empty, BucketName);
        }

        [ExpectedException(typeof(AmazonException))]
        [TestMethod]
        public void SetSecretAccessKey_With_SecretAccessKey_Null_Return_AmazonException()
        {
            var amazonClientNative = new AmazonClient(AccessKey,
                null, BucketName);
        }

        [ExpectedException(typeof(AmazonException))]
        [TestMethod]
        public void SetBucketName_With_BucketName_Empty_Return_AmazonException()
        {
            var amazonClientNative = new AmazonClient(AccessKey,
                SecretAccessKey, string.Empty);
        }

        [ExpectedException(typeof(AmazonException))]
        [TestMethod]
        public void SetBucketName_With_BucketName_Null_Return_AmazonException()
        {
            var amazonClientNative = new AmazonClient(AccessKey,
                SecretAccessKey, null);
        }

        [TestMethod]
        [ExpectedException(typeof(AmazonException))]
        public void Put_With_WriteObjectRequest_Null_Return_AmazonException()
        {
            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
                amazonClient.Put(null);
        }

        [TestMethod]
        public void Put_WriteObjectRequest_With_Source_Base64()
        {
            var writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, _baseSource64);

            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
                amazonClient.Put(writeObjectRequest);
        }

        [TestMethod]
        public void Put_WriteObjectRequest_With_Acl()
        {
            var writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, _baseSource64);
            writeObjectRequest.SetAcl( Acl.PublicRead);
            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
                amazonClient.Put(writeObjectRequest);
        }

        [TestMethod]
        public void Put_WriteObjectRequest_With_Encryption()
        {
            var writeObjectRequest = new WriteObjectRequest(Helpers.NameFile, Helpers.Extension, _baseSource64);
            writeObjectRequest.SetEncyption();
            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
                amazonClient.Put(writeObjectRequest);
        }

        [TestMethod]
        [ExpectedException(typeof(AmazonException))]
        public void Get_With_ObjectRequest_Null_Return_AmazonException()
        {
            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
            {
                var result = amazonClient.Get(null);
            }
        }

        [TestMethod]
        public void Get_ObjectRequest_Check_Source()
        {
            InsertResource(Helpers.NameFile, Helpers.Extension);
            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
            {
                var result = amazonClient.Get(_objectRequest);
                Assert.IsNotNull( result);
                Assert.IsTrue( result.Length > 0);
            }
        }

        [TestMethod]
        public void GetContentsByBucket_Check_Key()
        {
            InsertResource(Helpers.NameFile, Helpers.Extension);
            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
            {
                var listObjects = amazonClient.GetContentsByBucket();
                Assert.IsTrue( listObjects.Count > 0);
            }
        }

        [TestMethod]
        public void DeleteObjects_Remove_Folder()
        {
            InsertResource( string.Format( "images/{0}", Helpers.NameFile), Helpers.Extension);
            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
            {
                amazonClient.DeleteObjects( "images");

                var listObject = amazonClient.GetContentsByBucket("images");
                Assert.IsTrue( listObject.Count == 0);
            }
        }

        [TestMethod]
        [ExpectedException(typeof( AmazonException))]
        public void Delete_With_ObjectRequest_Null_Return_AmazonException()
        {
            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
            {
                amazonClient.Delete( null);
            }
        }

        [TestMethod]
        public void Delete_ObjectRequest()
        {
            InsertResource(Helpers.NameFile, Helpers.Extension);
            DeleteResource();

            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
            {
                var result = amazonClient.Get(_objectRequest);
                Assert.IsNull( result);
            }
        }

        private void DeleteResource()
        {
            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
                amazonClient.Delete(_objectRequest);
        }

        private void InsertResource(string partialPath, string extension )
        {
            using (var memoryStream = new MemoryStream())
            {
                var image = new Bitmap(_pathResourceJpg);
                image.Save(memoryStream, ImageFormat.Jpeg);

                var fakeObjectRequest = new WriteObjectRequest(partialPath, extension, memoryStream.ToArray());

                using (var amazonClient = AmazonClientFactory.CreateAmazonClient(_amazonSettings))
                {
                    amazonClient.Put(fakeObjectRequest);
                }
            }
        }
    }
}