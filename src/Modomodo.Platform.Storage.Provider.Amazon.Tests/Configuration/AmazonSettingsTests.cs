﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Platform.Stack.Interfaces;
using Modomodo.Platform.Storage.Provider.Amazon.Configuration;

namespace Modomodo.Platform.Storage.Provider.Amazon.Tests.Configuration
{
    [TestClass]
    public class AmazonSettingsTests
    {
        private readonly AmazonSettings _amazonSettings;

        public AmazonSettingsTests()
        {
            _amazonSettings = new AmazonSettings();
        }

        [TestMethod]
        public void AmazonSettings_Should_Be_Of_Type_IConfiguration()
        {
            Assert.IsInstanceOfType(_amazonSettings, typeof(IConfiguration<AmazonSettings>));
        }

        [TestMethod]
        public void GetSection_Not_Should_Be_Null()
        {
            var section = _amazonSettings.GetSection();
            Assert.IsNotNull( section);
        }

        [TestMethod]
        public void Check_Value_UserName()
        {
            var username = _amazonSettings.GetSection().UserName;
            Assert.IsTrue( username.Equals("username"));
        }

        [TestMethod]
        public void Check_Value_AccessKey()
        {
            var accessKey = _amazonSettings.GetSection().AccessKey;
            Assert.IsTrue( accessKey.Equals( "accesskey"));
        }

        [TestMethod]
        public void Check_Value_SecretAccessKey()
        {
            var secretAccessKey = _amazonSettings.GetSection().SecretAccessKey;
            Assert.IsTrue(secretAccessKey.Equals("secretaccesskey"));
        }

        [TestMethod]
        public void Check_Value_BucketName()
        {
            var bucketName = _amazonSettings.GetSection().BucketName;
            Assert.IsTrue(bucketName.Equals("bucketname"));
        }
    }
}