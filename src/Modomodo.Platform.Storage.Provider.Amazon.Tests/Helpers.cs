﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using Modomodo.Platform.Core.HelperUnitTest;

namespace Modomodo.Platform.Storage.Provider.Amazon.Tests
{
    internal class Helpers
    {
        public const string SectionEon = "Eon";
        public const string SectionAmazonDefault = "AmazonSettings";

        public const string NameFile = "Image_Jpg";
        public const string Extension = ".jpg";

        public const string MediaType = "custom/mediatype";

        public const string SampleBase64 = "Tm9uQW5kYXRlTmVsQmFnbm9Sb3Nzb19CeV9DaXB1";

        public static string GetPartialPath
        {
            get
            {
                return string.Format(@"\Resources\{0}{1}", NameFile, Extension); 
            }
        }

        public static string PathResource(string partialPath)
        {
            var pathProject = Utility.GetPathProjectRunning(Assembly.GetExecutingAssembly());
            return pathProject + partialPath;
        }

        public static string Base64(string partialPath)
        {
            string base64;
            var path = PathResource(partialPath);
            using (var memoryStream = new MemoryStream())
            {
                var image = new Bitmap(path);
                image.Save(memoryStream, ImageFormat.Jpeg);
                base64 = Convert.ToBase64String(memoryStream.ToArray());
            }
            return base64;
        }
    }
}